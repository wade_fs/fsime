package com.wade.fsime.layout.builder;

public class KeyboardLayoutException extends Exception {
    public KeyboardLayoutException(String message) {
        super(message);
    }
}
