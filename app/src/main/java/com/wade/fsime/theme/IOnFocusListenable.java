package com.wade.fsime.theme;

public interface IOnFocusListenable {
    public void onWindowFocusChanged(boolean hasFocus);
}
